package Utils;

import Entity.Person;
import org.junit.jupiter.api.Test;

import static Utils.Welcome.WelcomeOut;
import static org.junit.jupiter.api.Assertions.assertEquals;


class WelcomeTest {

    @Test
    public void OutMessage() throws Exception {

        String userName = "ME :v";  // Read user input
        String phoneNumber = "311";  // Read user phone input
        String age = "25";  // Read user old input


        Person person = new Person(userName,phoneNumber,age);

        String welcomeMessage = WelcomeOut(person);
        String expected = "Bienvenido señor " + "ME :v" +
                ", es un placer para nosotros contar con una persona de " + "25" + " años. \n" +
                "Próximamente nos comunicaremos con usted al numero " + "311" + ". \n" +
                "Feliz día";

        assertEquals(expected,welcomeMessage);
    }

}