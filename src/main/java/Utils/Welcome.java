package Utils;

import Entity.Person;

public class Welcome {
    public static String WelcomeOut(Person person) {
        return "Bienvenido señor " + person.getUserName() +
                ", es un placer para nosotros contar con una persona de " + person.getAge() + " años. \n" +
                "Próximamente nos comunicaremos con usted al numero " + person.getPhoneNumber() + ". \n" +
                "Feliz día";
    }
}
