import Entity.Person;

import java.util.Scanner;

import static Utils.Welcome.WelcomeOut;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner attributePerson = new Scanner(System.in);  // Create a Scanner object

        System.out.println("Enter username");
        String userName = attributePerson.nextLine();  // Read user input

        System.out.println("Enter phone number, \nOnly positive numbers \n10 characters");
        String phoneNumber = attributePerson.nextLine();  // Read user phone input

        System.out.println("Enter your age, \nOnly positive numbers");
        String age = attributePerson.nextLine();  // Read user old input

        Person person = new Person(userName,phoneNumber,age);

        System.out.println(WelcomeOut(person));
    }

}
