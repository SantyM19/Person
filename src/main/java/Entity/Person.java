package Entity;

public class Person {
    private String userName ;
    private String phoneNumber ;
    private String age ;

    public Person(String userName, String phoneNumber, String age) throws Exception {
        this.userName = userName;
        this.age = Number(age);
        this.phoneNumber = phoneNumber(phoneNumber);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public static String phoneNumber(String number) {
        try{
            if (number.length() == 10){
                return number;
            } else{
                throw new IllegalArgumentException(" Please Write your phoneNumber with 10 numbers \n");
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return number;
    }

    public static String Number(String number) {
        try {
            if (((Object)Integer.valueOf (number)).getClass().getSimpleName().equals("Integer") && Integer.parseInt(number) > 0){
                return number;
            }
            else {
                throw new IllegalArgumentException(" Please Write a Positive Number \n");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage() + " Please Write Numbers");
        }
        return number;
    }
}
